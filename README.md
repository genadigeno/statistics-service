# Accident Management Service(AMS)

## Project Description
Accident Management Service(AMS) is the project implemented with Event-Driven Architectural approach </br>
that provides messages distribution across several services based on their event type, `ACCIDENT_TYPE`. </br>
It is simulation of what happens when some kind of accident happens, </br>
it can be criminal, car, fire or some kind of accident that are saved in some </br>
(in case of default settings: `accident.events`) source topic.</br>

### Visual
![alt text](./materials/AMS-Project-Architecture.png)

## Statistics Service application description
Statistics Service application reads statistics events (in case of default settings it reads them from a topic:   </br> 
`statistics.events`) that are aggregated into `StatisticalModel` by event stream application. The aggregation is  </br>
performed by kafka streams windowing operation that is based on <b>windowed start time</b>, <b>windowed end time</b> </br>
and <b>key</b> (`AccidentType`). The aggregated data are processed and stored in  postgres SQL database.          </br> 
If some error arises in process, the application sends it to dead letter topic (in case of default settings:</br>
`statistics.events-dlt`) and continues the process for other ones.

![alt text](./materials/windowed.png)

## Getting started
### Installation requirements
In order to start up the application you need have installed:
- Docker on you machine </br>
- git </br>
  or
- Apache kafka cluster with at least three brokers
- git
- maven 3.9.x
- JDK-17

### Installation steps
The project installation could be done using docker-compose.yml via command line interface (CMD):
```
git clone https://gitlab.com/genadigeno/statistics-service.git && 
cd statistics-service && 
docker compose up
```
or
```
git clone https://gitlab.com/genadigeno/statistics-service.git && 
cd statistics-service && 
mvn clean package
```
```
java -DPOSTGRES_URL=jdbc:postgresql://localhost:5432/postgres -DPOSTGRES_USER=postgres -DPOSTGRES_PASSWORD= -jar ./target/statistics-service.jar
```
### JVM Parameters
- `SERVER_PORT` - application port number, default: 8080
- `KAFKA_MAIN_TOPIC_NAME` - kafka topic name for statistics service, default: statistics.events
- `KAFKA_DLT_TOPIC_NAME` - kafka dead letter topic name for statistics service application, default: statistics.events-dlt
- `KAFKA_BOOTSTRAP_SERVERS` - kafka cluster url, default: localhost:9092,localhost:9093
- `SCHEMA_REGISTRY_HOST` - schema registry url, default: http://localhost:8081
- `POSTGRES_URL` - postgres url, default: jdbc:postgresql://localhost:5432/postgres
- `POSTGRES_USER` - postgres user, default: postgres
- `POSTGRES_PASSWORD` - postgres password

***
### Used Technologies
- <img src="./materials/eda.png" height="15" alt="img"> Event-Driven Architecture
- <img src="./materials/java.png" height="15" alt="img"> Java 17
- <img src="./materials/boot.png" height="15" alt="img"> Spring Boot & Spring Boot JPA
- <img src="./materials/kafka.png" height="15" alt="img"> Apache Kafka & Kafka Streams library
- <img src="./materials/docker.png" height="15" alt="img"> Docker
- <img src="./materials/git.png" height="15" alt="img"> Git
- <img src="./materials/gitlab-ci-cd.png" height="15" alt="img"> Gitlab CI/CD
### Project status
Completed
