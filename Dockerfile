FROM openjdk:17-alpine

RUN mkdir "app"
WORKDIR /app

RUN mkdir "logs"
VOLUME $PWD/logs

COPY ./target/statistics-service.jar .

ENV SERVER_PORT=8080
ENV KAFKA_MAIN_TOPIC_NAME="statistics.events"
ENV KAFKA_BOOTSTRAP_SERVERS="localhost:9092,localhost:9093"
ENV KAFKA_SCHEMA_REGISTRY_URL="http://localhost:8081"
ENV KAFKA_DLT_TOPIC_NAME="statistics.events-dlt"
ENV POSTGRES_URL="jdbc:postgresql://localhost:5432/postgres"
ENV POSTGRES_USER="postgres"
ENV POSTGRES_PASSWORD=""

EXPOSE ${SERVER_PORT}

ENTRYPOINT ["java", "-jar"]

CMD ["statistics-service.jar"]