package ams.statistics.jpa;

import lombok.*;

import javax.persistence.*;

@Table(name = "statistical_models", schema = "public")
@Entity
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticalModelData {
    @EmbeddedId
    private WindowedId id;
    private long count;
}
