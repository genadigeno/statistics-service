package ams.statistics.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatisticalModelRepository extends CrudRepository<StatisticalModelData, Long> {
}
