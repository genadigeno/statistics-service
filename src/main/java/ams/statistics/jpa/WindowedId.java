package ams.statistics.jpa;

import ams.data.model.AccidentType;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class WindowedId implements Serializable {
    @Column(name = "window_end")
    private LocalDateTime end;
    @Column(name = "window_start")
    private LocalDateTime start;
    @Enumerated(EnumType.STRING)
    private AccidentType type;
}
